# finnys-foods-android

> Check out how easy it is to get started building a custom Android application which leverages the Salesforce
> platform and the Salesforce Mobile SDK.

Feature team developers may not be aware of the [Salesforce Mobile SDK](https://developer.salesforce.com/docs/atlas.en-us.mobile_sdk.meta/mobile_sdk/intro.htm), 
which lets you harness the power of the Salesforce Platform within stand-alone mobile apps. In this short series, I’ll 
demonstrate how to use the Salesforce Mobile SDK for custom mobile applications written in React Native, Android Studio, 
and Xcode. This repository will be focused on the native Android implementation.

A graphical representation of this new series could be summarized as shown below:

![Salesforce Mobile SDK](./images/Leveraging-Salesforce-Mobile-SDK.png)

## Publications

This repository is related to an article published on DZone.com:

* [Exploring the Salesforce Mobile SDK Using Android Studio](https://dzone.com/articles/exploring-the-salesforce-mobile-sdk-using-android)

To read more of my publications, please review one of the following URLs:

* https://dzone.com/users/1224939/johnjvester.html
* https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=1224939


## Finny’s Foods - An Example Use Case

Having a son (Finny) born with special needs introduced a personal desire to gain an understanding and appreciation of 
the products we utilize in our daily life. For the years before Finny was born, I felt like my wife (Nicole) and I were 
living a healthy lifestyle. That reality received a much-needed wake up call as Nicole started diving deep into the list 
of ingredients included in the foods that were a part of our daily diet.

Fast-forward to today, and you will find our diet contains very few processed foods, no gluten, low sugar, and very 
little dairy. As much as possible, our protein originates from grass-fed sources, and we always favor organic options. 
Don’t get me started on GMO.

For this series, let’s consider a Finny’s Foods application which provides a simple list of meals that are both 
acceptable to us and favored by Finny. For now, we will include two simple attributes for each meal:

* Name of the meal
* Rating (1 to 5, where a rating of 5 is an absolute favorite for Finny)

Over time, I plan to introduce other attributes (e.g. summary, ingredients, directions, and even a photo), but let’s walk before we run.


## Getting Started with Android Studio

In order to get started, please follow the steps noted in the following link:

https://trailhead.salesforce.com/en/content/learn/projects/mobilesdk_setup_dev_tools/mobilesdk_setup_android

These steps will include making sure Android Studio, Android SDKs, at least one Android emulator (AVD) and the `forcedroid` CLI are all installed.

Next, I executed `forcedroid` from a terminal session with the following options:

```shell
forcedroid create
Enter your application type (native_kotlin or native, leave empty for native_kotlin): native
Enter your application name: finnys-foods-android
Enter your package name: com.gitlab.johnjvester.finnysfoods.android
Enter your organization name (Acme, Inc.): JVC
Enter output directory for your app (leave empty for the current directory):
```

Once completed, I started Android Studio and imported the new `finnys-foods-android` project as documented in the following URL:

https://developer.salesforce.com/docs/atlas.en-us.noversion.mobile_sdk.meta/mobile_sdk/android_new_project.htm

## Creating the Meal Object in Salesforce

Before I could get started on creating a new application in Android Studio, I needed to establish the `Meal` object in Salesforce.

I already had a Developer org that I could use (you can create a new one [here](https://developer.salesforce.com/docs/atlas.en-us.externalidentityImplGuide.meta/externalidentityImplGuide/external_identity_create_developer_org.htm)), 
so I simply logged in using my email address and password for that org. Next, I navigated to the **Apps | App Manager** and 
the Setup perspective in Salesforce.

I created a new Lightning App called **Meal**:

![New Lightning App](./images/New-Lightning-App.png)

On the remaining screens in the setup, I went with all of the default settings, and I granted access to all 
Salesforce User Profiles.

Next, I visited the **Objects & Fields | Object Manager** menu option in the **Settings** perspective of Salesforce. Once 
I located the Meal item that was just created, I used the drop-down component to Edit the object.

I switched from the **Details** submenu to the **Fields & Relationships** option. I quickly realized I did not need to 
create a **Name** property for my Meal object, since Salesforce already took care of that for me. I just needed to add 
the **Rating** field.

Using the **New** button, I selected the number field type and populated **Step Two** as noted below:

![Rating Field](./images/AddRatingField.png)

I used the default values and saved my new field. Now, I can use both the name and rating fields in my React Native application.

![Meal Object](./images/MealObject.png)

Using the Salesforce client, I went ahead and populated some source data to use for development of the React Native 
application. Below is a summary of the values submitted:

![Sample Data](./images/SampleData.png)

## Finny’s Foods In Action

Within Android Studio, I used the **Run** menu option and selected the **Run ‘app’** option, which also maps 
to the Shift-F10 shortcut. 

First time use will redirect to a Salesforce login screen automatically, where I used the same email address and password 
to access my Developer org. Once signed in, the Finny’s Foods application was displayed, including the five star rating 
value for each item:

![Finny's Foods App Screen Shot (Android)](./images/FinnysFoodsScreenAndroid.png)

## Additional Information

Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.







